<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width">
      <title>Supermusic ID | Coming Soon</title>
      <meta name="description" content=""/>
      <link rel="shortcut icon" href="favicon.png">
      <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Teko" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
      <style type="text/css">
         /*
         * Globals
         */
         /* Links */
         a,
         a:focus,
         a:hover {
         color: #fff;
         }
         /* Custom default button */
         .btn-default {
         color: #fff;
         text-shadow: none;
         /* Prevent inheritence from `body` */
         background-color: transparent;
         border: 2px solid #fff;
         border-radius: 20px;
         padding: 0.5rem 2rem;
         }
         .btn-default:hover,
         .btn-default:focus {
         background-color: rgba(255, 255, 255, 0.3);
         }
         /*
         * Base structure
         */
         body {
         background: url('bgred.jpg') no-repeat;
         background-size: cover;
         background-attachment: fixed;
         color: #fff;
         text-align: center;
         font-family: 'Permanent Marker';
         }
         #section-maintenance {
            display: flex;
            align-items: center;
            justify-content: center;
            min-height: 70vh;
         }
         .heading {
         font-size: 120px;
         }
         .lead {
         font-family: 'Permanent Marker';
         font-size: 45px;
         }
         /* Extra markup and styles for table-esque vertical and horizontal centering */
         .site-wrapper {
         height: 100%;
         }
         .site-wrapper-inner {
         vertical-align: top;
         height: 100%;
         }
         .cover-container {
         margin-right: auto;
         margin-left: auto;
         }
         /* Padding for spacing */
         .inner {
         padding: 30px;
         }
         .footer-txt {
         font-size: 20px;
         }
         /*
         * Header
         */
         .masthead-brand {
         margin-top: -20px;
         margin-bottom: 40px;
         }
         .nav-masthead {
         text-align: center;
         display: block;
         }
         .nav-masthead .nav-link {
         display: inline-block;
         }
         @media (min-width: 768px) {
         .masthead-brand {
         float: left;
         }
         .nav-masthead {
         float: right;
         }
         }
         /*
         * Cover
         */
         .cover {
         padding: 0 20px;
         }
         .cover .btn-notify {
         padding: 10px 60px;
         font-weight: 500;
         text-transform: uppercase;
         border-radius: 40px;
         }
         .cover-heading {
         text-transform: uppercase;
         letter-spacing: 10px;
         font-size: 40px;
         margin-bottom: 20px;
         font-family: Cinzel;
         }
         @media (min-width: 768px) {
         .cover-heading {
         font-size: 40px;
         letter-spacing: 15px;
         }
         }
         .cover-copy {
         max-width: 1000px;
         margin: 0 auto 3rem;
         text-align: center;
         }
         /*
         * Footer
         */
         .mastfoot {
         color: #fff;
         font-family: 'Teko';
         }
         /*
         * Subscribe modal box
         */
         #subscribeModal .modal-content {
         background-color: #212121;
         color: #fff;
         text-align: left;
         }
         #subscribeModal .modal-header, #subscribeModal .modal-footer {
         border: 0;
         }
         #subscribeModal .close {
         color: #fff;
         }
         #subscribeModal .form-control {
         margin-top: 1rem;
         background: rgba(0, 0, 0, 1);
         color: #fff;
         }
         #subscribeModal .form-control:focus {
         border-color: #49506a;
         }
         /*
         * Affix and center
         */
         @media (min-width: 768px) {
         /* Pull out the header and footer */
         .masthead {
         top: 0;
         }
         .mastfoot {
         bottom: 0;
         }
         /* Start the vertical centering */
         .site-wrapper-inner {
         vertical-align: middle;
         }
         /* Handle the widths */
         .masthead,
         .mastfoot,
         .cover-container {
         width: 100%;
         /* Must be percentage or pixels for horizontal alignment */
         }
         }
         @media (max-width: 1920px){
         .heading {
            font-size: 80px;
         }
         .lead {
            font-size: 25px;
         }
         .cover-copy {
            max-width: 800px;
         }

         }
         @media (min-width: 992px) {
         .masthead,
         .mastfoot,
         .cover-container {
         width: 100%;
         }
         }
         .countdown {
         display: -webkit-inline-box;
         margin-left: -15px;
         }
         .counter-box {
         padding-top: 35px;
         border: 3px solid #fff;
         border-radius: 50%;
         margin-right: 40px;
         width: 150px;
         height: 150px;
         }
         @media (max-width: 480px){
         body { 
         background-size: cover;
         }
         .heading {
         font-size: 30px;
         }
         .lead {
         font-size: 20px;
         }
         .cover-container {
         max-width: 100%;
         }
         .countdown {
         display: -webkit-inline-box!important;
         }
         .counter-box {
         margin-bottom: 20px;
         width: 55px;
         height: 55px;
         margin-right: 10px;
         padding-top: 20px;
         }
         #days, #hours, #minutes, #seconds {
         font-size: 18px!important;
         }
         .cover-heading {
         font-size: 30px;
         }
         }
         @media (max-width: 768px){
         .cover-container {
         max-width: 100%;
         }
         .countdown {
         display: -webkit-inline-box!important;
         }
         .counter-box {
         margin-bottom: 20px;
         width: 65px;
         height: 65px;
         margin-right: 10px;
         padding-top: 16px;
         }
         #days, #hours, #minutes, #seconds {
         font-size: 18px!important;
         }
         .cover-heading {
         font-size: 30px;
         }
         .heading {
            font-size: 40px;
         }
         .lead {
            font-size: 20px;
         }
         #section-maintenance {
             min-height: 60vh;
         }

         }
      </style>
   </head>
   <body id="top">
      <div class="site-wrapper">
         <div class="site-wrapper-inner">
            <div class="cover-container">
               <div class="masthead clearfix">
                  <div class="inner">
                     <h3 class="masthead-brand"><img src="logo.png" width="250px;"></h3>
                     <nav class="nav nav-masthead">
                        <a class="nav-link nav-social" href="https://www.facebook.com/SuperMusicID/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                        <a class="nav-link nav-social" href="https://twitter.com/supermusic_id?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a> 
                        <a class="nav-link nav-social" href="https://www.instagram.com/supermusic_id/?hl=en" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a> 
                     </nav>
                  </div>
               </div>
            </div>
            <div id="section-maintenance">
               <div class="inner cover">
                  <h1 class="heading">We'll Be Back Soon!</h1>
                  <p class="lead cover-copy">Sorry for the inconvenience but we're performing some maintenance at the moment. otherwise we'll be back online shortly!</p>
               </div>
            </div>
            <footer class="mastfoot">
               <div class="inner">
                  <p class="footer-txt">&copy; supermusic.id</p>
               </div>
            </footer>
         </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
   </body>
</html>